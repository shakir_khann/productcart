import React, { Component } from "react";

export default class Counter extends Component {
  render() {
    const { onIncrement, onDecrement, onDelete, product } = this.props;
    return (
      <div className="row mt-3">
        <div className="col-1">
          <span className={this.getClasses}>{this.formatCount}</span>
        </div>
        <div className="col-11">
          <button
            className="btn-secondary btn btn-sm mr-2"
            onClick={() => onIncrement(product)}
          >
            +
          </button>
          <button
            disabled={product.value === 0 && true}
            className="btn-secondary btn btn-sm mr-2"
            onClick={() => onDecrement(product)}
          >
            -
          </button>
          <button
            className="btn-danger btn btn-sm"
            onClick={() => onDelete(product)}
          >
            x
          </button>
        </div>
      </div>
    );
  }

  get getClasses() {
    const { value } = this.props.product;
    let classes = "badge-pill m-2 p-2 badge-";
    classes += value === 0 ? "warning" : "primary";
    return classes;
  }

  get formatCount() {
    const { value } = this.props.product;
    return value === 0 ? "zero" : value;
  }
}
