import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  render() {
    const {
      products,
      onDelete,
      onIncrement,
      onDecrement,
      onReset
    } = this.props;
    return (
      <div>
        <main className="mt-3">
          <div>
            <button className="btn btn-primary btn-md m-2" onClick={onReset}>
              Reset
            </button>
          </div>
          <div>
            {products.map(product => (
              <Counter
                key={product.id}
                product={product}
                onIncrement={onIncrement}
                onDecrement={onDecrement}
                onDelete={onDelete}
              />
            ))}
          </div>
        </main>
      </div>
    );
  }
}

export default Counters;
