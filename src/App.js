import React from "react";
import Counters from "./components/counters";
import NavBar from "./components/navBar";

export default class App extends React.Component {
  state = {
    products: [
      { id: 1, value: 0 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 }
    ]
  };

  handleIncrement = product => {
    const products = [...this.state.products];
    const index = products.indexOf(product);
    product = { ...products[index] };
    product.value++;
    products[index] = product;
    this.setState({ products });
  };

  handleDecrement = product => {
    const products = [...this.state.products];
    const index = products.indexOf(product);
    product = { ...products[index] };
    product.value--;
    products[index] = product;
    this.setState({ products });
  };

  handleReset = () => {
    let products = [...this.state.products];
    products = products.map(p => ({ id: p.id, value: 0 }));
    this.setState({ products });
  };

  handleDelete = product => {
    const products = this.state.products.filter(p => p !== product);

    this.setState({ products });
  };

  render() {
    return (
      <React.Fragment>
        <NavBar count={this.state.products.filter(p => p.value > 0).length} />
        <Counters
          products={this.state.products}
          onIncrement={this.handleIncrement}
          onDecrement={this.handleDecrement}
          onReset={this.handleReset}
          onDelete={this.handleDelete}
        />
      </React.Fragment>
    );
  }
}
